import {BrowserRouter as Router} from 'react-router-dom'
import {Container} from 'react-bootstrap'
import {Routes, Route} from 'react-router-dom'
import AppNavbar from './components/Navbar'
import Home from './Pages/home'
import Login from './Pages/login'
import Signup from './Pages/signup'
import './App.css';


function App() {
  return( 
    <Router>
    <AppNavbar/>
    <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/signup" element={<Signup/>}/>
            
          </Routes>
          </Container>
    </Router>
  )
}

export default App;
