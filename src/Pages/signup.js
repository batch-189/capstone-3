import React from 'react'
import {Link} from 'react-router-dom'
import {useState} from 'react'
import {Container, Row, Col, Form, Button} from 'react-bootstrap'



export default function Signup(){

	const[email,setEmail]= useState('')
	const[password1,setPassword1]= useState('')
	const[password2, setPassword2] = useState('')
	const[firstName, setFirstName] = useState('')
	const[lastName, setLastName] = useState('')
	const[mobileNo, setMobileNo] = useState('')

	return(

			<Row>
				<Col md={6} className="loginUser container">
					<Form class="mt-3" >
						<h1>Signup your account</h1>
						
				<Form.Group className="mb-3" controlId="firstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Enter First Name"
			        value={firstName}
			        onChange={e => {
			        	setFirstName(e.target.value)
			        	
			        }} 
			        required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="lastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Enter Last Name"
			        value={lastName}
			        onChange={e => {
			        	setLastName(e.target.value)
			        	
			        }} 
			        required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="mobileNo">
		        <Form.Label>Mobile Number</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Enter Mobile Number"
			        value={mobileNo}
			        onChange={e => {
			        	setMobileNo(e.target.value)
			        	
			        }} 
			        required/>
		      </Form.Group>



					<Form.Group>
							<Form.Label>Email Address</Form.Label>
							<Form.Control
							type="email"
							placeholder="Enter email"
							value={email}
							onChange={(e)=> setEmail(e.target.value)}
							required />
							
						</Form.Group>

						<Form.Group>
							<Form.Label>Password</Form.Label>
							<Form.Control
							type="password"
							placeholder="Enter Password"
							value={password1}
							onChange={(e)=> setPassword1(e.target.value)}
							required />
							
						

				<Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		        type="password"
		        value={password2} 
		        onChange={e => setPassword2(e.target.value)}
		        placeholder="Password" />
		      </Form.Group>

		      


		      </Form.Group>
					<Form.Group className="mt-3">
						<Button type="submit" id="submitBtn">Submit</Button>
					</Form.Group>
			  </Form>
			</Col>
			 <Col md={6} className="signup-image container">
			 	<img src="images/model.png"/>

			</Col>

			</Row>

		)

}