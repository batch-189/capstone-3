import React from 'react'
import {useState} from 'react'
import {Link} from 'react-router-dom'
import {Container,Row, Col, Form, Button} from 'react-bootstrap'




export default function Login(){

		const[email,setEmail]= useState('')
		const[password,setPassword]= useState('')


		function sumbitRegister(){

		}



		return(
			<Row>
				<Col md={6} className="loginUser container">
					<Form class="mt-3" >
						<h1>Login to your account</h1>
						<Form.Group>
							<Form.Label>Email Address</Form.Label>
							<Form.Control
							type="email"
							placeholder="Enter email"
							value={email}
							onChange={(e)=> setEmail(e.target.value)}
							required />
							
						</Form.Group>

						<Form.Group>
							<Form.Label>Password</Form.Label>
							<Form.Control
							type="password"
							placeholder="Enter Password"
							value={password}
							onChange={(e)=> setPassword(e.target.value)}
							required />
							
						</Form.Group>
						<Form.Group className="mt-3">
								<Button type="submit" id="submitBtn">Login</Button>
						</Form.Group>
						<p className="mt-2">Don't have an account? <Link to="/signup">Create account</Link></p>
					</Form>
				</Col>
				 <Col md={6} className="login-image container">
				 	
				 </Col>

			</Row>

			)



}